import React, {useState, useEffect} from "react";
import { useHistory } from 'react-router';
import {useAuth} from "../../store/Auth";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
   wrapper: {
       display: 'flex',
       flexDirection: 'column',
       justifyContent: 'center',
       alignItems: 'center',
       height: '100vh'
   },
    title: {
       marginBottom: '20px'
    },
    content: {
       width: '30%',
        textAlign: 'center',
        marginBottom: '10%'
    },
    button: {
       marginTop: '20px'
    },
    formError: {
       color: 'DarkRed',
        textAlign: 'left'
    }
}));

export const Login = () => {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const history = useHistory();
    const {authenticate, isAuthenticated, authError} = useAuth();
    const styles = useStyles();

    useEffect(() => {
        if (isAuthenticated) {
            history.push('/contacts')
        }
    }, [isAuthenticated, history]);

    const onLogin = (event) => {
        event.preventDefault();
        authenticate({login: login, password: password})
    };

    return (
        <Box className={styles.wrapper}>
            <Container className={styles.content} maxWidth={false}>
                <Typography className={styles.title} component="h3" variant="h3">Login</Typography>
                <form onSubmit={onLogin}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        label="Email Address"
                        onChange={event => setLogin(event.target.value)}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        label="Password"
                        type="password"
                        onChange={event => setPassword(event.target.value)}
                    />
                    {
                        authError &&
                            <Typography className={styles.formError} component="h6" variant="h6">
                                * {authError}
                            </Typography>
                    }
                    <Button
                        className={styles.button}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                    >
                        LOGIN
                    </Button>
                </form>
            </Container>
        </Box>
    )
};
