import React, {useState} from "react";
import { makeStyles } from '@material-ui/core/styles';
import {Header} from "./Header";
import {useContacts} from "../../store";
import {Contact} from "./Contact";
import {NewContactButton} from "./NewContactButton";
import {ContactDialog} from "./ContactDialog";
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(() => ({
    wrapper: {
        display: 'flex',
        flexDirection: 'column',
        height: '100vh'
    },
    content: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    contacts: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        width: '450px',
        padding: '20px 100px 50px 100px'
    },
    searchText: {
        height: '100px',
        display: 'flex',
        alignItems: 'center'
    },
    backdrop: {
        color: '#fff',
        zIndex: 999
    }
}));



export const Contacts = () => {
    const styles = useStyles();
    const {contacts, isSearching, searchQuery, searchedContacts, loading} = useContacts();
    const [dialogIsVisible, setDialogIsVisible] = useState(false);
    const [contactIsEditing, setContactIsEditing] = useState(false);
    const [currentContactId, setCurrentContactId] = useState(-1);

    const createContact = () => {
        setContactIsEditing(false);
        setDialogIsVisible(true);
    };

    const editContact = (currentContactId) => {
        setContactIsEditing(true);
        setCurrentContactId(currentContactId);
        setDialogIsVisible(true);
    };

    const closeDialog = () => {
        setDialogIsVisible(false);
    };

    return (
        <div className={styles.wrapper}>
            <Header/>
            <Box className={styles.content}>
                <Box className={styles.contacts}>
                    <NewContactButton createContact={createContact}/>
                    {contacts && contacts.map(contact => (
                        <Contact
                            id={contact.id}
                            title={contact.title}
                            content={contact.content}
                            editContact={editContact}
                        />
                    ))}
                </Box>
                {isSearching && <Box className={styles.contacts}>
                    <Box className={styles.searchText}>
                        <Typography component="h4" variant="h4">Search results for {searchQuery}:</Typography>
                    </Box>
                    {searchedContacts.length > 0
                        ? searchedContacts.map(contact => (
                            <Contact
                                id={contact.id}
                                title={contact.title}
                                content={contact.content}
                                editContact={editContact}
                            />))
                        : <Typography component="h4" variant="h4">Nothing found</Typography>
                    }
                </Box>}
            </Box>
            <ContactDialog
                open={dialogIsVisible}
                editing={contactIsEditing}
                contactId={currentContactId}
                closeDialog={closeDialog}
            />
            <Backdrop className={styles.backdrop} open={loading}>
                <CircularProgress color="inherit" />
            </Backdrop>
        </div>
    )
};
