import React, {useState} from "react";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import Divider from '@material-ui/core/Divider';
import InputBase from '@material-ui/core/InputBase';
import Paper from '@material-ui/core/Paper';
import ClearIcon from '@material-ui/icons/Clear';
import {makeStyles} from "@material-ui/core";
import {useContacts} from "../../../store";

const useStyles = makeStyles(() => ({
    paper: {
        width: '500px',
        padding: '2px 0 2px 10px',
        display: 'flex',
        alignItems: 'center',
    },
    input: {
        flex: 1
    }
}));

export const SearchField = () => {
    const styles = useStyles();
    const [query, setQuery] = useState('');
    const {doSearch, clearSearch} = useContacts();

    const onClear = () => {
        setQuery('');
        clearSearch();
    };

    return (
        <Paper className={styles.paper}>
            <InputBase
                className={styles.input}
                placeholder="Enter contact title for search"
                onChange={event => setQuery(event.target.value)}
                value={query}
            />
            <IconButton onClick={onClear}>
                <ClearIcon/>
            </IconButton>
            <Divider orientation="vertical" flexItem/>
            <IconButton onClick={() => doSearch(query)}>
                <SearchIcon/>
            </IconButton>
        </Paper>
    )
};
