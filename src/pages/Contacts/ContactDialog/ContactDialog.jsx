import React, {useState, useEffect} from "react";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {useContacts} from "../../../store";

export const ContactDialog = ({open, editing, closeDialog, contactId}) => {
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const {contacts, addContact, updateContact} = useContacts();

    useEffect(() => {
        if (open && editing) {
            const contact = contacts.find(contact => contact.id === contactId);
            setTitle(contact.title);
            setContent(contact.content);
        }
    }, [open, editing, contactId, contacts]);

    const onClose = () => {
        setContent('');
        setTitle('');
        closeDialog();
    };

    const onSubmit = (event) => {
        event.preventDefault();
        editing ? updateContact(contactId, title, content) : addContact(title, content);
        onClose();
    };

    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>{editing ? 'Editing contact' : 'New contact'}</DialogTitle>
            <form onSubmit={onSubmit}>
                <DialogContent>
                    <TextField
                        margin="dense"
                        label="Title"
                        fullWidth
                        onChange={event => setTitle(event.target.value)}
                        value={title}
                        required
                    />
                    <TextField
                        margin="dense"
                        label="Content"
                        fullWidth
                        onChange={event => setContent(event.target.value)}
                        value={content}
                        required
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={onClose} color="primary">
                        Cancel
                    </Button>
                    <Button type="submit" color="primary">
                        Submit
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    )
};
