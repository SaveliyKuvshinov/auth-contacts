import React from "react";import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import ClearIcon from '@material-ui/icons/Clear';
import {useContacts} from "../../../store";

const useStyles = makeStyles((theme) => ({
    paper: {
        width: '100%',
        margin: '20px 0 0 0',
    },
    grid: {
        minHeight: '100%',
        padding: '20px'
    }
}));


export const Contact = ({id, title, content, editContact}) => {
    const styles = useStyles();
    const {deleteContact} = useContacts();

    return (
        <Paper className={styles.paper}>
            <Grid className={styles.grid} container direction="row" justify='space-between' alignItems='center'>
                <Grid justify='space-between' wrap="nowrap" container direction="column" xs={9}>
                    <Grid item>
                        <Typography component="h4" variant="h4" noWrap>
                            {title}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography component="h6" variant="h6" noWrap>
                            {content}
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container direction="row" xs>
                    <IconButton onClick={() => editContact(id)}>
                        <EditIcon color="primary"/>
                    </IconButton>
                    <IconButton onClick={() => deleteContact(id)}>
                        <ClearIcon color="primary"/>
                    </IconButton>
                </Grid>
            </Grid>
        </Paper>
    )
};
