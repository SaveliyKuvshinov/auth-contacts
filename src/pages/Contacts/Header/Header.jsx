import React from "react";
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import {useAuth} from "../../../store/Auth";
import { useHistory } from 'react-router';
import {SearchField} from "../SearchField";

const useStyles = makeStyles(() => ({
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        minHeight: '15%',
        backgroundColor: '#3f51b5',
        padding: '0 50px 0 50px',
        color: 'white'
    },
    button: {
        color: 'white'
    },
    logoutWrapper: {
        display: 'flex',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    }
}));

export const Header = () => {
    const {logout, userLogin} = useAuth();
    const history = useHistory();
    const styles = useStyles();

    const onLogout = () => {
        logout();
        history.push('/login');
    };

    return (
        <div className={styles.wrapper}>
            <Typography component="h4" variant="h4">
                Contacts
            </Typography>
            <SearchField/>
            <div className={styles.logoutWrapper}>
                <Typography>
                    Logged in as {userLogin}
                </Typography>
                <Button
                    className={styles.button}
                    variant="text" href=''
                    size="large"
                    onClick={onLogout}
                >
                    LOGOUT
                </Button>
            </div>
        </div>
    )
};
