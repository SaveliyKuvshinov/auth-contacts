import React from "react";
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import Box from '@material-ui/core/Box';
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    wrapper: {
        width: '100%',
        textAlign: 'center',
        height: '100px'
    },
    icon: {
        fontSize: '4rem'
    }
}));

export const NewContactButton = ({createContact}) => {
    const styles = useStyles();

    return (
        <Box className={styles.wrapper}>
            <IconButton onClick={createContact}>
                <AddIcon className={styles.icon} color="primary"/>
            </IconButton>
        </Box>
    )
};
