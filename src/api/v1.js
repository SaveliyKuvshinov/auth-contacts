import {SERVER_PORT, SERVER_URL} from "../settings";

const authenticate = (credentials) => {
    return fetch(`${SERVER_URL}:${SERVER_PORT}/users`)
        .then(async response => {
            if (!response.ok || !response.body) {
                throw new Error('Response is not ok');
            }

            const data = await response.json();
            const user = data.find(user => user.login === credentials.login && user.password === credentials.password);

            return user ?
                {
                    authenticated: true,
                    authError: '',
                    token: user.token,
                    userId: user.id,
                    userLogin: user.login
                } :
                {
                    authenticated: false,
                    authError: 'Invalid credentials',
                    token: null
                }
        })
        .catch(error => ({
            authenticated: false,
            authError: error.message,
            token: null
        }))
};

const getUserByToken = (token) => {
    return fetch(`${SERVER_URL}:${SERVER_PORT}/users?token=${token}`)
        .then(async response => {
            if (!response.ok || !response.body) {
                throw new Error('Response is not ok');
            }

            const users = await response.json();
            return {
                success: true,
                data: users[0]
            }
        })
        .catch(error => ({
            success: false,
            error: error.message
        }))
};

const getContacts = (userId) => {
    return fetch(`${SERVER_URL}:${SERVER_PORT}/users/${userId}/contacts`)
        .then(async response => {
            if (!response.ok || !response.body) {
                throw new Error('Response is not ok');
            }

            const data = await response.json();
            return {
                success: true,
                data: data
            }
        })
        .catch(error => ({
            success: false,
            error: error.message
        }))
};

const addContact = (userId, title, content) => {
    return fetch(`${SERVER_URL}:${SERVER_PORT}/contacts`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({title: title, content: content, userId: userId})
    })
        .then(async response => {
            if (!response.ok || !response.body) {
                throw new Error('Response is not ok');
            }

            const data = await response.json();
            return {
                success: true,
                data: data
            }
        })
        .catch(error => ({
            success: false,
            error: error.message
        }));
};

const updateContact = (contactId, userId, title, content) => {
    return fetch(`${SERVER_URL}:${SERVER_PORT}/contacts/${contactId}`, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({title: title, content: content, userId: userId})
    })
        .then(async response => {
            if (!response.ok || !response.body) {
                throw new Error('Response is not ok');
            }

            const data = await response.json();
            return {
                success: true,
                data: data
            }
        })
        .catch(error => ({
            success: false,
            error: error.message
        }));
};

const deleteContact = (contactId) => {
    return fetch(`${SERVER_URL}:${SERVER_PORT}/contacts/${contactId}`, {
        method: 'DELETE'
    })
        .then(async response => {
            if (!response.ok || !response.body) {
                throw new Error('Response is not ok');
            }

            const data = await response.json();
            return {
                success: true,
                data: data
            }
        })
        .catch(error => ({
            success: false,
            error: error.message
        }));
};

export const api = {
    authenticate,
    getUserByToken,
    getContacts,
    addContact,
    updateContact,
    deleteContact
};
