import React from "react";
import {Route} from "react-router";

export const PrivateRoute = ({ isAuthenticated, ...props }) =>
    isAuthenticated
    ? <Route { ...props } />
    : <div>Для доступа необходимо авторизоваться</div>;
