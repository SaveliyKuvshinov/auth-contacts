import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import {Login} from "../pages/Login";
import {Contacts} from "../pages/Contacts";
import {PrivateRoute} from "./PrivateRoute";
import {useAuth} from "../store/Auth";

export const Router = () => {
    const {isAuthenticated} = useAuth();

    return (
        <BrowserRouter>
            <Switch>
                <Route
                    path="/"
                    render={() =>
                        (isAuthenticated ? <Redirect to="/contacts"/> : <Redirect to="/login"/>)
                    }
                    exact
                />
                <Route path="/login" component={Login} exact />
                <PrivateRoute isAuthenticated={isAuthenticated} path="/contacts" component={Contacts} exact />
            </Switch>
        </BrowserRouter>
    )
};
