import React, {useContext, useState, useCallback, useEffect} from "react";
import {api} from "../../api/v1";
import {useAuth} from "../Auth";

const ContactsContext = React.createContext(null);

export const useContacts = () => useContext(ContactsContext);

export const ContactsProvider = (props) => {
    const {userId} = useAuth();
    const [contacts, setContacts] = useState([]);
    const [searchedContacts, setSearchedContacts] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const [isSearching, setIsSearching] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        api.getContacts(userId)
            .then(result => {
                if (result.success) {
                    setContacts(result.data);
                    setIsSearching(false);
                    setLoading(false);
                }
                else {
                    window.alert(result.error)
                }
            })
            .catch(error => error)
    },[userId]);

    useEffect(() => {
        setSearchedContacts(contacts.filter(contact => contact.title.indexOf(searchQuery) === 0))
    }, [contacts, searchQuery]);

    const addContact = useCallback((title, content) => {
        api.addContact(userId, title, content)
            .then(result => {
                if (result.success) {
                    setContacts([...contacts, result.data]);
                }
                else {
                    window.alert(result.error);
                }
            })
            .catch(error => error)
    },[userId, contacts]);

    const updateContact = useCallback((contactId, title, content) => {
        api.updateContact(contactId, userId, title, content)
            .then(result => {
                if (result.success) {
                    setContacts([...contacts.filter(contact => contact.id !== contactId), result.data]);
                }
                else {
                    window.alert(result.error);
                }
            })
            .catch(error => error)
    },[userId, contacts]);

    const deleteContact = useCallback(contactId => {
        api.deleteContact(contactId)
            .then(result => {
                if (result.success) {
                    setContacts(contacts.filter(contact => contact.id !== contactId));
                }
                else {
                    window.alert(result.error);
                }
            })
            .catch(error => error)
    }, [contacts]);

    const doSearch = useCallback(query => {
        setIsSearching(true);
        setSearchQuery(query);
        setSearchedContacts(contacts.filter(contact => contact.title.indexOf(query) === 0))
    }, [contacts]);

    const clearSearch = useCallback(() => {
        setIsSearching(false);
        setSearchQuery('');
        setSearchedContacts([])
    }, []);

    const values = {
        contacts,
        searchedContacts,
        addContact,
        updateContact,
        deleteContact,
        doSearch,
        clearSearch,
        searchQuery,
        isSearching,
        loading
    };

    return (
        <ContactsContext.Provider value={values}>
            {props.children}
        </ContactsContext.Provider>
    )
};
