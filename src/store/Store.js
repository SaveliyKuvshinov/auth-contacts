import React from "react";
import {AuthProvider} from "./Auth";
import {ContactsProvider} from "./Contacts";

export const StoreProvider = (props) => {
    return (
        <AuthProvider>
            <ContactsProvider>
                {props.children}
            </ContactsProvider>
        </AuthProvider>
    )
};
