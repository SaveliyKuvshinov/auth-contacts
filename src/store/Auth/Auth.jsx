import React, {useContext, useState, useCallback, useEffect} from "react";
import {api} from "../../api/v1";

const AuthContext = React.createContext(null);

export const useAuth = () => useContext(AuthContext);

export const AuthProvider = (props) => {
    const [isAuthenticated, setIsAuthenticated] = useState(!!sessionStorage.getItem('token'));
    const [userId, setUserId] = useState(-1);
    const [userLogin, setUserLogin] = useState('');
    const [authError, setAuthError] = useState('');

    const authenticate = useCallback(credentials => {
        api.authenticate(credentials)
            .then(authResult => {
                setAuthError(authResult.authError);
                if (authResult.authenticated) {
                    sessionStorage.setItem('token', authResult.token);
                    setIsAuthenticated(authResult.authenticated);
                    setUserId(authResult.userId);
                    setUserLogin(authResult.userLogin)
                }
            });
    }, []);

    useEffect(() => {
        if (isAuthenticated && userId === -1) {
            api.getUserByToken(sessionStorage.getItem('token'))
                .then(result => {
                    if (result.success) {
                        setUserId(result.data.id);
                        setUserLogin(result.data.login);
                    }
                    else {
                        window.alert(result.error);
                    }
                })
        }
    },[isAuthenticated, userId]);



    const logout = useCallback(() => {
        sessionStorage.clear();
        setIsAuthenticated(false);
    }, []);

    const values = {
        isAuthenticated,
        authError,
        authenticate,
        logout,
        userId,
        userLogin
    };

    return (
        <AuthContext.Provider value={values}>
            {props.children}
        </AuthContext.Provider>
    )
};
